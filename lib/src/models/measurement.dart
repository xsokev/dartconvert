import './definition.dart';
import './anchor.dart';

abstract class Measurement {
  Definition metric;
  Definition imperial;

  Map<String, Anchor> anchors;
}