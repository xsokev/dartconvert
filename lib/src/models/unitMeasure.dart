import './measurement.dart';
import './unit.dart';

class UnitMeasure {
  final String measure;
  final String system;
  final Measurement measurement;
  final Unit unit;

  UnitMeasure({
    this.measure,
    this.system, 
    this.measurement, 
    this.unit, 
  });
}