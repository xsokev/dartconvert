class Unit {
  final String abbr;
  final String abbrPlural;
  final String name;
  final String namePlural;
  final double toAnchor;
  final double anchorShift;
  final Function transform;

  Unit({
    this.abbr, 
    this.abbrPlural, 
    this.name, 
    this.namePlural, 
    this.toAnchor,
    this.anchorShift,
    this.transform
  });
}