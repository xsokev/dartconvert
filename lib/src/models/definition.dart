import './unit.dart';

class Definition {
  final List<Unit> units;

  Definition({this.units});

}