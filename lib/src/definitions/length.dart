import 'package:dart_convert/src/models/models.dart';

class Length implements Measurement {
  Definition metric = new Definition(
    units: [
      new Unit(
        abbr: 'mm',
        abbrPlural: 'mms',
        name: 'Millimeter',
        namePlural: 'Millimeters',
        toAnchor: 1/1000
      ),
      new Unit(
        abbr: 'cm',
        abbrPlural: 'cms',
        name: 'Centimeter',
        namePlural: 'Centimeters',
        toAnchor: 1/100
      ),
      new Unit(
        abbr: 'm',
        abbrPlural: 'ms',
        name: 'Meter',
        namePlural: 'Meters',
        toAnchor: 1.0
      ),
      new Unit(
        abbr: 'km',
        abbrPlural: 'kms',
        name: 'Kilometer',
        namePlural: 'Kilometers',
        toAnchor: 1000.0
      ),
    ]
  );
  Definition imperial = new Definition(
    units: [
      new Unit(
        abbr: 'in',
        abbrPlural: 'ins',
        name: 'Inch',
        namePlural: 'Inches',
        toAnchor: 1/12,
      ),
      new Unit(
        abbr: 'yd',
        abbrPlural: 'yds',
        name: 'Yard',
        namePlural: 'Yards',
        toAnchor: 3.0
      ),
      new Unit(
        abbr: 'ft-us',
        abbrPlural: 'ft-us',
        name: 'US Survey Foot',
        namePlural: 'US Survey Feet',
        toAnchor: 1.000002
      ),
      new Unit(
        abbr: 'ft',
        abbrPlural: 'ft',
        name: 'Foot',
        namePlural: 'Feet',
        toAnchor: 1.0
      ),
      new Unit(
        abbr: 'mi',
        abbrPlural: 'mis',
        name: 'Mile',
        namePlural: 'Miles',
        toAnchor: 5280.0
      ),
    ]
  );
  Map<String, Anchor> anchors = {
    'metric': new Anchor(unit: 'm', ratio: 3.28084),
    'imperial': new Anchor(unit: 'ft', ratio: 1/3.28084),
  };
}
