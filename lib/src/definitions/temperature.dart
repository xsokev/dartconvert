import 'package:dart_convert/src/models/models.dart';

class Mass implements Measurement {
  Definition metric = new Definition(
    units: [
      new Unit(
        abbr: 'mcg',
        abbrPlural: 'mcgs',
        name: 'Microgram',
        namePlural: 'Micrograms',
        toAnchor: 1/1000000
      ),
      new Unit(
        abbr: 'mg',
        abbrPlural: 'mgs',
        name: 'Milligram',
        namePlural: 'Milligrams',
        toAnchor: 1/1000
      ),
      new Unit(
        abbr: 'g',
        abbrPlural: 'gs',
        name: 'Gram',
        namePlural: 'Grams',
        toAnchor: 1.0
      ),
      new Unit(
        abbr: 'kg',
        abbrPlural: 'kgs',
        name: 'Kilogram',
        namePlural: 'Kilograms',
        toAnchor: 1000.0
      ),
      new Unit(
        abbr: 'mt',
        abbrPlural: 'mts',
        name: 'Metric Tonne',
        namePlural: 'Metric Tonnes',
        toAnchor: 1000000.0
      ),
    ]
  );
  Definition imperial = new Definition(
    units: [
      new Unit(
        abbr: 'oz',
        abbrPlural: 'ozs',
        name: 'Ounce',
        namePlural: 'Ounces',
        toAnchor: 1/16,
      ),
      new Unit(
        abbr: 'lb',
        abbrPlural: 'lbs',
        name: 'Pound',
        namePlural: 'Pounds',
        toAnchor: 1.0
      ),
      new Unit(
        abbr: 'st',
        abbrPlural: 'sts',
        name: 'Stone',
        namePlural: 'Stones',
        toAnchor: 14.0,
      ),
      new Unit(
        abbr: 't',
        abbrPlural: 'ts',
        name: 'Ton',
        namePlural: 'Tons',
        toAnchor: 2000.0
      ),
    ]
  );
  Map<String, Anchor> anchors = {
    'metric': new Anchor(unit: 'g', ratio: 1/453.592),
    'imperial': new Anchor(unit: 'lb', ratio: 453.592),
  };
}
