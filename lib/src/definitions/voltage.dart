import 'package:dart_convert/src/models/models.dart';

class Mass implements Measurement {
  Definition metric = new Definition(
    units: [
      new Unit(
        abbr: 'V',
        abbrPlural: 'Vs',
        name: 'Volt',
        namePlural: 'Volts',
        toAnchor: 1.0
      ),
      new Unit(
        abbr: 'mV',
        abbrPlural: 'mVs',
        name: 'Millivolt',
        namePlural: 'Millivolts',
        toAnchor: 1/1000
      ),
      new Unit(
        abbr: 'kV',
        abbrPlural: 'kVs',
        name: 'Kilovolt',
        namePlural: 'Kiltvolts',
        toAnchor: 100.0
      ),
    ]
  );
  Definition imperial = new Definition(
    units: []
  );
  Map<String, Anchor> anchors = {
    'metric': new Anchor(unit: 'V', ratio: 1.0),
  };
}
