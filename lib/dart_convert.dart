library dart_convert;

import 'package:dart_convert/src/definitions/definitions.dart';
import 'package:dart_convert/src/models/models.dart';

class DartConvert {
  final Map<String, Measurement> measurements = {
    'length': new Length(),
    'mass': new Mass(),
  };
  double value = 0.0;

  DartConvert(this.value);
  DartConvert.withDenominator(double numerator, double denominator) {
    this.value = numerator / denominator;
  }

  double convert(String from, {String to, int precision: 2}){
    if(to == null){
      to = from;
    }
    UnitMeasure source = _getUnit(from);
    UnitMeasure target = _getUnit(to);

    if(source.unit.abbr == target.unit.abbr){
      return value;
    }
    if(source.measure != target.measure){
      throw new CastError();
    }

    double result = this.value * source.unit.toAnchor;

    if(source.unit.anchorShift != null){
      result -= source.unit.anchorShift;
    }

    if(source.system != target.system){
      if(source.unit.transform != null){
        result = source.unit.transform(result);
      } else {
        Anchor anchor = source.measurement.anchors[source.system];
        result *= anchor.ratio;
      }
    }

    if (target.unit.anchorShift != null) {
      result += target.unit.anchorShift;
    }

    result = result / target.unit.toAnchor;

    return double.parse(result.toStringAsFixed(precision));
  }

  List<UnitMeasure> possibilities(String measure, String abbr){
    List<UnitMeasure> _possibilities = [];
    List<UnitMeasure> _list = list(measure);
    _list.forEach((UnitMeasure unitMeasure) {
      if(unitMeasure.unit.abbr != abbr) {
        _possibilities.add(unitMeasure);
      }
    });
    return _possibilities;
  }

  List<UnitMeasure> list(String measure){
    List<UnitMeasure> list = [];
    measurements.forEach((String m, Measurement measurement) {
      if(m == measure) {
        measurement.imperial.units.forEach((Unit unit){
          list.add(
            new UnitMeasure(
              measure: measure,
              measurement: measurement,
              system: 'imperial',
              unit: unit
            )
          );
        });
        measurement.metric.units.forEach((Unit unit){
          list.add(
            new UnitMeasure(
              measure: measure,
              measurement: measurement,
              system: 'metric',
              unit: unit
            )
          );
        });
      }
    });
    return list;
  }

  UnitMeasure _getUnit(abbr){
    UnitMeasure found;
    measurements.forEach((String measure, Measurement measurement) {
      measurement.imperial.units.forEach((Unit unit){
        if(unit.abbr == abbr){
          found = new UnitMeasure(
            measure: measure,
            measurement: measurement,
            system: 'imperial',
            unit: unit
          );
          return false;
        }
      });
      measurement.metric.units.forEach((Unit unit){
        if(unit.abbr == abbr){
          found = new UnitMeasure(
            measure: measure,
            measurement: measurement,
            system: 'metric',
            unit: unit
          );
          return false;
        }
      });
      if(found != null){
        return false;
      }
    });
    return found;
  }
}