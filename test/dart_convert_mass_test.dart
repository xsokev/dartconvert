import 'package:test/test.dart';

import 'package:dart_convert/dart_convert.dart';

void main() {
  test('converts imperial mass values to same unit', () {
    final convertor = new DartConvert(1.0);
    expect(convertor.convert('lb', to: 'lb'), 1.0);
    expect(convertor.convert('oz', to: 'oz'), 1.0);
    expect(convertor.convert('t', to: 't'), 1.0);
  });
  test('converts imperial mass values', () {
    final convertor = new DartConvert(1.0);
    expect(convertor.convert('lb', to: 'oz'), 16);
    expect(convertor.convert('lb', to: 't', precision: 4), 0.0005);
    expect(convertor.convert('oz', to: 'st', precision: 8), 0.00446429);
    expect(convertor.convert('t', to: 'lb'), 2000);
    expect(convertor.convert('st', to: 'lb'), 14);
  });
  test('converts metric mass values to same unit', () {
    final convertor = new DartConvert(1.0);
    expect(convertor.convert('g', to: 'g', precision: 12), 1);
    expect(convertor.convert('mg', to: 'mg'), 1);
    expect(convertor.convert('mcg', to: 'mcg', precision: 4), 1);
    expect(convertor.convert('kg', to: 'kg'), 1);
  });
  test('converts metric mass values', () {
    final convertor = new DartConvert(1.0);
    expect(convertor.convert('g', to: 'mg'), 1000);
    expect(convertor.convert('mg', to: 'g', precision: 3), 0.001);
    expect(convertor.convert('kg', to: 'g'), 1000);
    expect(convertor.convert('mt', to: 'kg'), 1000);
  });
  test('converts mass values across systems', () {
    final convertor = new DartConvert(1.0);
    expect(convertor.convert('g', to: 'oz', precision: 6), 0.035274);
    expect(convertor.convert('mt', to: 't', precision: 5), 1.10231);
    expect(convertor.convert('t', to: 'k  g', precision: 4), 907.184);
    expect(convertor.convert('lb', to: 'kg', precision: 6), 0.453592);
    expect(convertor.convert('lb', to: 'g', precision: 3), 453.592);
  });
}
