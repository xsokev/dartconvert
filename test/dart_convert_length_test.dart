import 'package:test/test.dart';

import 'package:dart_convert/dart_convert.dart';

void main() {
  test('converts imperial length values to same unit', () {
    final convertor = new DartConvert(1.0);
    expect(convertor.convert('ft', to: 'ft'), 1.0);
    expect(convertor.convert('in', to: 'in'), 1.0);
    expect(convertor.convert('yd', to: 'yd'), 1.0);
    expect(convertor.convert('ft-us', to: 'ft-us'), 1.0);
    expect(convertor.convert('mi', to: 'mi'), 1.0);
  });
  test('converts imperial length values', () {
    final convertor = new DartConvert(1.0);
    expect(convertor.convert('ft', to: 'ft-us', precision: 12), 0.999998000004);
    expect(convertor.convert('ft-us', to: 'ft', precision: 6), 1.000002);
    expect(convertor.convert('ft', to: 'in'), 12);
    expect(convertor.convert('in', to: 'ft', precision: 4), 0.0833);
    expect(convertor.convert('ft', to: 'mi', precision: 12), 0.000189393939);
    expect(convertor.convert('mi', to: 'ft'), 5280);
    expect(convertor.convert('yd', to: 'ft'), 3);
    expect(convertor.convert('ft', to: 'yd'), 0.33);
    expect(convertor.convert('mi', to: 'yd'), 1760);
    expect(convertor.convert('mi', to: 'in'), 63360);
    expect(convertor.convert('in', to: 'yd', precision: 7), 0.0277778);
  });
  test('converts metric length values to same unit', () {
    final convertor = new DartConvert(1.0);
    expect(convertor.convert('m', to: 'm', precision: 12), 1);
    expect(convertor.convert('cm', to: 'cm'), 1);
    expect(convertor.convert('mm', to: 'mm', precision: 4), 1);
    expect(convertor.convert('km', to: 'km'), 1);
  });
  test('converts metric length values', () {
    final convertor = new DartConvert(1.0);
    expect(convertor.convert('m', to: 'cm', precision: 4), 100);
    expect(convertor.convert('cm', to: 'm'), 0.01);
    expect(convertor.convert('m', to: 'mm'), 1000);
    expect(convertor.convert('km', to: 'cm'), 100000);
  });
  test('converts length values across systems', () {
    final convertor = new DartConvert(1.0);
    expect(convertor.convert('ft', to: 'cm'), 30.48);
    expect(convertor.convert('ft', to: 'm', precision: 4), 0.3048);
    expect(convertor.convert('m', to: 'yd', precision: 4), 1.0936);
    expect(convertor.convert('km', to: 'mi', precision: 6), 0.621371);
    expect(convertor.convert('mi', to: 'km', precision: 5), 1.60934);
  });
}
